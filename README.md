# Route Planning Project

This repo contains the starter code for the Route Planning project.

<img src="map.png" width="600" height="450" />

## Cloning

When cloning this project, be sure to use the `--recurse-submodules` flag. Using HTTPS:
```
git clone https://github.com/udacity/CppND-Route-Planning-Project.git --recurse-submodules
```
or with SSH:
```
git clone git@github.com:udacity/CppND-Route-Planning-Project.git --recurse-submodules
```

## Dependencies for Running Locally
* cmake >= 3.11.3
  * All OSes: [click here for installation instructions](https://cmake.org/install/)
* make >= 4.1 (Linux, Mac), 3.81 (Windows)
  * Linux: make is installed by default on most Linux distros
  * Mac: [install Xcode command line tools to get make](https://developer.apple.com/xcode/features/)
  * Windows: [Click here for installation instructions](http://gnuwin32.sourceforge.net/packages/make.htm)
* gcc/g++ >= 7.4.0
  * Linux: gcc / g++ is installed by default on most Linux distros
  * Mac: same instructions as make - [install Xcode command line tools](https://developer.apple.com/xcode/features/)
  * Windows: recommend using [MinGW](http://www.mingw.org/)
* IO2D
  * Installation instructions for all operating systems can be found [here](https://github.com/cpp-io2d/P0267_RefImpl/blob/master/BUILDING.md)
  * This library must be built in a place where CMake `find_package` will be able to find it

## Compiling and Running
### Setting up build environment
- Build depends on an old version of GCC and cmake and catch. Tried my darndess to get my arch linux machine to get in configuration but through my hands up and used docker

-  This version of ubuntu had the correct version of GCC
```
sudo docker run -it --rm -v $(pwd):/build ubuntu:focal
```
- Had to install pakcages to get io2d to build
```
apt update
apt install build-essential cmake libcairo2-dev libgraphicsmagick1-dev libpng-dev
```
- clone and build io2d example [here](https://github.com/cpp-io2d/P0267_RefImpl/blob/master/BUILDING.md)

- After following those instructgions need to make in the debug direcotry then navigate to: ```P0267_RefImpl/Debug/P0267_RefImpl/P0267_RefImpl```
```
make install
```

### Compiling

To compile the project, first, create a `build` directory and change to that directory:
```
mkdir build && cd build
```
From within the `build` directory, then run `cmake` and `make` as follows:
```
cmake ..
make
```
### Running
The executable will be placed in the `build` directory. From within `build`, you can run the project as follows:
```
./OSM_A_star_search
```
Or to specify a map file:
```
./OSM_A_star_search -f ../<your_osm_file.osm>
```

## Testing

The testing executable is also placed in the `build` directory. From within `build`, you can run the unit tests as follows:
```
./test
```

## Things I actaully did in this project
The project was really a shell to implemnet the A* algorythum. The primary work that I did on it was to complete TODOS in /src/route_planner.cpp. This first class ended up giving me the c++ background to start the software side of an FPGA CPU combo project at work.
